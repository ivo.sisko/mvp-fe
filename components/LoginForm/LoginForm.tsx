import React, { ReactElement, useState } from 'react';
import { Form, Formik, FormikHelpers } from 'formik';
import * as Yup from 'yup';

import { FieldInput } from '../CustomInput/CustomInput';
import { Button } from '../Button';
import { useRouter } from 'next/router';
import { useUserContext } from '../../providers';

interface FormData {
  email: string;
  password: string;
}
interface ILoginFormProps {
  className?: string;
  handleOnConfirm?: () => void; //for modal purposes, to close the modal after submit
  isModal?: boolean;
}

export default function LoginForm({}: ILoginFormProps): ReactElement {
  const router = useRouter();
  const { login } = useUserContext();
  return (
    <React.Fragment>
      <div className={` flex flex-col bg-white`}>
        <Formik
          initialValues={{
            email: '',
            password: '',
          }}
          validationSchema={Yup.object().shape({
            email: Yup.string()
              .email('Email invalid')
              .required('Required field'),
            password: Yup.string().required('Required field'),
          })}
          onSubmit={async (formData) => {
            await login!(formData);
            router.push('/');
          }}
        >
          {(props) => {
            const { isSubmitting, errors, submitForm, submitCount } = props;
            return (
              <Form
                className="w-full"
                noValidate={true}
                onKeyDown={(e) => {
                  if (e.key === 'Enter') {
                    submitForm();
                  }
                }}
              >
                <FieldInput
                  id="email"
                  className="w-full max-w-input"
                  name="email"
                  type="email"
                  label={'Email'}
                  error={(submitCount && errors.email) || ''}
                />
                <FieldInput
                  className="w-full max-w-input mt-2"
                  name="password"
                  type="password"
                  label={'Pasword'}
                  error={(submitCount && errors.password) || ''}
                  id={'password'}
                />

                <div>
                  <Button type="submit" className="bg-green-400 mt-4">
                    Login
                  </Button>
                  <Button href="/registration" type="button" className="text-black">
                    Register
                  </Button>
                </div>
              </Form>
            );
          }}
        </Formik>
      </div>
    </React.Fragment>
  );
}
