import React from 'react';

export const CustomLabel: React.FC<React.HTMLProps<HTMLButtonElement>> = ({
	htmlFor,
	className,
	children
}) => (
	<label
		htmlFor={htmlFor}
		className={`
        ${className || ''} 
        block text-md
    `}
	>
		{children}
	</label>
);
