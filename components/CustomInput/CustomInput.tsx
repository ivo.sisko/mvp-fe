import { Field } from 'formik';
import React, { useState, useEffect } from 'react';
import { CustomLabel } from './CustomLabel';

export interface ICustomInputProps
  extends React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > {
  id: string;
  name: string;
  value?: string;
  label?: string;
  placeholder?: string;
  className?: string;
  inputBorder?: boolean;
  error?: string;
  info?: string;
  rounded?: 'default' | 'lg' | 'full';
}

const CustomInput: React.FC<ICustomInputProps> = (props) => {
  const {
    id,
    name,
    className,
    inputBorder = true,
    type,
    label,
    value,
    placeholder,
    onChange,
    onBlur,
    onFocus,
    error,
    rounded = 'default',
    info,
  } = props;
  const [innerValue, setInnerValue] = React.useState<string | undefined>('');
  const [isFocused, setIsFocused] = useState<boolean>(false);
  // const [isHidden, setIsHidden] = useState<boolean>(true);

  useEffect(() => {
    if (value) setInnerValue(value);
  }, [value]);

  // const toggleIsHidden = () => {
  // 	setIsHidden(!isHidden);
  // };

  const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInnerValue(e.target.value);
    onChange && onChange(e);
  };

  const handleOnFocus = (e: React.FocusEvent<HTMLInputElement>) => {
    setIsFocused(true);
    onFocus && onFocus(e);
  };

  const handleOnBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    setIsFocused(false);
    onBlur && onBlur(e);
  };

  const roundedClasses: any = {
    default: 'rounded',
    lg: 'rounded-lg',
    full: 'rounded-full',
  };

  return (
    <div className={`${className ? className : ''}`}>
      {label && (
        <CustomLabel className="mb-2" htmlFor={name}>
          {label}
        </CustomLabel>
      )}
      {info && <p className="mb-3 text-base">{info}</p>}
      <input
        className={`w-full  px-5 ${
          inputBorder
            ? 'border placeholder-gray-400  border-trueGray-400 '
            : 'border-none'
        }  outline-none h-14 text-md ${roundedClasses[rounded]}`}
        id={id}
        type={type}
        // type={type === 'password' ? (isHidden && 'password') || 'text' : type}
        name={name}
        value={innerValue}
        onChange={handleOnChange}
        onFocus={handleOnFocus}
        onBlur={handleOnBlur}
        placeholder={placeholder}
      />
      {/* {type === 'password' && value && value !== '' && (
					<button
						onClick={toggleIsHidden}
						type="button"
						className="ml-20 mr-20 text-xs text-gray-600 underline bg-transparent border-none outline-none hover:text-primary"
					>
						Prikaži
					</button>
				)} */}
      {error && <InputFeedback error={error} />}
    </div>
  );
};

const InputFeedback = ({ error }: { error: string }) =>
  error ? <p className="mt-3 text-sm font-bold text-red-400">{error}</p> : null;

export const FieldInput: React.FC<ICustomInputProps> = (props) => {
  const { className, name, ref, ...rest } = props;
  return (
    <Field name={name}>
      {(props: any) => {
        const {
          field: { name, value, onChange },
        } = props;
        return (
          <CustomInput
            className={className}
            ref={ref as any}
            name={name}
            value={value}
            onChange={(e) => onChange(e)}
            {...rest}
          />
        );
      }}
    </Field>
  );
};

export default CustomInput;
