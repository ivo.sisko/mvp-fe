import React from 'react';
import { genres } from '../../constants/genres';
import CustomInput from '../CustomInput';

interface IFilterProps {
  onFilterChange: (filter: any) => void;
  className?: string;
}

const Filter: React.FC<IFilterProps> = ({ onFilterChange, className = '' }) => {
  const [filter, setFilter] = React.useState({
    genre: '',
    year: '',
  });

  const handleFilterChange = (e) => {
    setFilter({
      ...filter,
      [e.target.name]: e.target.value,
    });
  };

  React.useEffect(() => {
    onFilterChange(filter);
  }, [filter]);
  return (
    <div className={`${className} flex sm:px-5 gap-10 sm:gap-5`}>
      <select
        className="text-gray-400 font-semibold"
        onChange={handleFilterChange}
        name="genre"
      >
        <option value="">All genres</option>
        {genres.map((genre) => (
          <option key={genre.id} value={genre.id}>
            {genre.name}
          </option>
        ))}
      </select>
      <input
        className="w-32 text-gray-400 font-semibold px-2 border-b"
        placeholder="Filter by year"
        type="number"
        min="1950"
        max="2021"
        step="1"
        name="year"
        onChange={handleFilterChange}
      />
    </div>
  );
};

export default Filter;
