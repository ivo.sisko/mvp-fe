import Link from 'next/link';
interface IButtonProps {
  children: React.ReactNode;
}
export const Button: React.FC<
  IButtonProps & React.HTMLProps<HTMLButtonElement>
> = ({ children, className, href, onClick }) => {
  return href ? (
    <Link href={href}>
      <a
        className={`${className} px-6 py-3 text-white hover:opacity-90 rounded-full font-bold`}
      >
        {children}
      </a>
    </Link>
  ) : (
    <button
      onClick={onClick}
      className={`${className} px-6 py-3 text-white hover:opacity-90 rounded-full font-bold`}
    >
      {children}
    </button>
  );
};
