import Link from 'next/link';
import React from 'react';
import { useDebounce } from '../../hooks';

interface ISearchProps {
  onQueryChange: (q: string) => void;
}

const Search: React.FC<ISearchProps> = ({ onQueryChange }) => {
  const [query, setQuery] = React.useState('');
  const debouncedQuery = useDebounce({ value: query, delay: 500 });

  React.useEffect(() => {
    onQueryChange(debouncedQuery);
  }, [debouncedQuery]);

  return (
    <div className="pt-2 sm:mx-auto text-gray-600 shadow-xl rounded-xl relative w-full sm:w-80   my-2">
      <input
        className="border-2 border-gray-100 bg-white h-14 px-5  rounded-lg text-sm sm:text-base focus:outline-none w-full"
        type="search"
        name="search"
        placeholder="Search"
        onChange={(e) => setQuery(e.target.value)}
      />
    </div>
  );
};

export default Search;
