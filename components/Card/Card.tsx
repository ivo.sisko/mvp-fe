import Image from 'next/image';
import React from 'react';
import { genres as genreList, genres } from '../../constants/genres';

interface ICardProps {
  title: string;
  genres: number[];
  releaseDate: string;
  slotEnd: any;
  rate: number;
}

const getGenreNamesFromIds = (genreIds: number[]) => {
  return genres
    .filter((genre) => genreIds.includes(genre.id))
    .map((genre) => genre.name);
};

const Card: React.FC<ICardProps> = ({
  title,
  genres,
  releaseDate,
  slotEnd,
  rate,
}) => {
  return (
    <div className="py-3 sm:max-w-xl sm:mx-auto shadow-lg rounded-lg">
      <div className="flex ">
        <img
          className="w-1/3 rounded-xl"
          src="https://image.tmdb.org/t/p/w600_and_h900_bestv2/nAU74GmpUk7t5iklEp3bufwDq4n.jpg"
          alt="A Quiet Place movie poster"
        />
        <div className="w-2/3 flex flex-col justify-between p-4">
          <h3 className="text-2xl font-medium">{title}</h3>
          <div>
            <h4 className="text-gray-400 font-semibold py-2">
              {new Date(releaseDate).toLocaleDateString('hr-HR')}
            </h4>
            <div className="flex flex-wrap gap-1 font-semibold text-base text-gray-400 mb-2">
              {getGenreNamesFromIds(genres).map((genre) => (
                <span
                  key={genre}
                  className="mr-3 bg-gray-100 px-2 rounded-full"
                >
                  {genre}
                </span>
              ))}
            </div>
          </div>
          <h4 className="font-semibold text-gray-400 mb-2">Rating: {rate}</h4>
          {slotEnd()}
        </div>
      </div>
    </div>
  );
};

export default Card;
