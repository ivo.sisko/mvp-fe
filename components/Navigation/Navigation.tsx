import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';
import { useWindowSize } from '../../hooks';
import { useUserContext } from '../../providers/User.provider';

import { Button } from '../Button';

interface INavigationProps {}

const Navigation: React.FC<INavigationProps> = ({}) => {
  const router = useRouter();
  const isLoginPage = /login/i.test(router.route);
  const { authenticated, logout } = useUserContext();
  const size = useWindowSize();
  if (isLoginPage) {
    return null;
  }
  return (
    <nav className="flex justify-between pl-6 sm:px-16  py-4 shadow-md items-center">
      <div className="font-bold text-xl tracking-widest text-orange-500 ">
        <Link href="/">Logo</Link>
      </div>
      {authenticated ? (
        <ul className="flex gap-10 text-lg items-center ">
          <li className="hidden sm:block">
            <Link href="/">Home</Link>
          </li>
          <li>
            <Link href="/favourites">
              {size.width < 768 ? (
                <a className="flex items-center">
                  <Image src="/heart.svg" height="20px" width="20px" />
                </a>
              ) : (
                'Favourites'
              )}
            </Link>
          </li>
          <li>
            {size.width < 768 ? (
              <Button className="flex items-center" onClick={logout}>
                <Image src="/logout.svg" height="20px" width="20px" />
              </Button>
            ) : (
              <Button onClick={logout} className="bg-gray-300">
                Logout
              </Button>
            )}
          </li>
        </ul>
      ) : (
        <div>
          <Link href="/login">Login</Link>
        </div>
      )}
    </nav>
  );
};

export default Navigation;
