import React from 'react';

interface ISortProps {
  onSortChange: (s: string) => void;
  className?: string;
}

const Sort: React.FC<ISortProps> = ({ onSortChange, className = '' }) => {
  const [value, setValue] = React.useState('');
  const [direction, setDirection] = React.useState('asc');

  React.useEffect(() => {
    function handleSort() {
      if (value !== '') {
        onSortChange(`${value}_${direction}`);
        return;
      }
      onSortChange('');
    }
    handleSort();
  }, [value, direction]);
  return (
    <div className={className}>
      <select
        onChange={(e) => setValue(e.target.value)}
        className="bg-white sm:px-5 text-gray-400 font-semibold"
      >
        <option value="">Sort by</option>
        <option value="name">Name</option>
        <option value="year">Release date</option>
      </select>
      {value !== '' && (
        <select
          className="bg-white px-5 text-gray-400 font-semibold"
          onChange={(e) => setDirection(e.target.value)}
        >
          <option value="asc">ASC</option>
          <option value="desc">DESC</option>
        </select>
      )}
    </div>
  );
};

export default Sort;
