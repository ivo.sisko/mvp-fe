import React, { ReactElement, useState } from 'react';
import { Form, Formik, FormikHelpers } from 'formik';
import * as Yup from 'yup';

import { FieldInput } from '../CustomInput/CustomInput';
import { Button } from '../Button';
import { useRouter } from 'next/router';
import { useUserContext } from '../../providers';
import { register } from '../../services/auth/register';

interface FormData {
  email: string;
  password: string;
  firstname: string;
  lastname: string;
}
interface IRegistrationFormProps {
  className?: string;
  isModal?: boolean;
}

export default function RegistrationForm({}: IRegistrationFormProps): ReactElement {
  const router = useRouter();
  return (
    <React.Fragment>
      <div className={` flex flex-col bg-white`}>
        <Formik
          initialValues={{
            email: '',
            password: '',
            firstname: '',
            lastname: '',
          }}
          validationSchema={Yup.object().shape({
            email: Yup.string()
              .email('Email invalid')
              .required('Required field'),
            password: Yup.string().required('Required field'),
            firstname: Yup.string().required('Required field'),
            lastname: Yup.string().required('Required field'),
          })}
          onSubmit={async (formData) => {
            await register!(formData, () => router.push('/login'));
          }}
        >
          {(props) => {
            const { errors, submitForm, submitCount } = props;
            return (
              <Form
                className="w-full"
                noValidate={true}
                onKeyDown={(e) => {
                  if (e.key === 'Enter') {
                    submitForm();
                  }
                }}
              >
                <FieldInput
                  id="firstname"
                  className="w-full max-w-input"
                  name="firstname"
                  type="firstname"
                  label={'Firstname'}
                  error={(submitCount && errors.firstname) || ''}
                />
                <FieldInput
                  className="w-full max-w-input mt-2"
                  name="lastname"
                  type="lastname"
                  label={'Lastname'}
                  error={(submitCount && errors.lastname) || ''}
                  id={'lastname'}
                />
                <FieldInput
                  id="email"
                  className="w-full max-w-input"
                  name="email"
                  type="email"
                  label={'Email'}
                  error={(submitCount && errors.email) || ''}
                />
                <FieldInput
                  className="w-full max-w-input mt-2"
                  name="password"
                  type="password"
                  label={'Pasword'}
                  error={(submitCount && errors.password) || ''}
                  id={'password'}
                />

                <div>
                  <Button type="submit" className="bg-green-400 mt-4">
                    Register
                  </Button>
                </div>
              </Form>
            );
          }}
        </Formik>
      </div>
    </React.Fragment>
  );
}
