import React, { useEffect, useState } from 'react';

type DebounceProps = {
  value: string;
  delay: number;
};

function useDebounce({ value, delay }: DebounceProps) {
  const [debouncedValue, setDebouncedValue] = useState<any>(value);

  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);

  return debouncedValue;
}

export { useDebounce };
