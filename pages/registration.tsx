import React from 'react';
import RegistrationForm from '../components/RegistrationForm';

const Registration: React.FC = () => {
  return (
    <div className="m-auto p-12 sm:p-40 max-w-4xl">
      <RegistrationForm />
    </div>
  );
};

export default Registration;
