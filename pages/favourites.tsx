import React from 'react';
import { useQueryClient } from 'react-query';
import Card from '../components/Card';
import { useGetFavourites } from '../services/favourites/getFavorites';
import { removeFromFavourites } from '../services/favourites/removeFromFavourites';

interface IFavouritesProps {}

const Favourites: React.FC<IFavouritesProps> = ({}) => {
  const { data: movies } = useGetFavourites();
  const queryClient = useQueryClient();
  const handleRemoveFromFavourites = async (id: string) => {
    await removeFromFavourites(id);
    queryClient.invalidateQueries('favourites');
  };

  return (
    <main className="flex flex-col">
      <div className="flex flex-wrap gap-3 p-4">
        {movies?.length === 0 ? (
          <div className="text-lg text-gray-400 font-bold text-center w-full p-20">
            There are no favourite movies.
          </div>
        ) : (
          movies?.map((movie) => (
            <Card
              key={movie.id}
              title={movie.title}
              genres={movie.genre_ids}
              rate={movie.vote_average}
              releaseDate={movie.release_date}
              slotEnd={() => (
                <button
                  onClick={() => handleRemoveFromFavourites(String(movie.id))}
                  className="py-2 px-2 sm:px-6 sm:py-3 text-white bg-blue-400 hover:opacity-90 rounded-full font-bold"
                >
                  Remove
                </button>
              )}
            />
          ))
        )}
      </div>
    </main>
  );
};

export default Favourites;
