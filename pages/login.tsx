import React from 'react';
import LoginForm from '../components/LoginForm';

const login: React.FC = ({}) => {
  return (
    <div className="m-auto p-12 sm:p-40 max-w-4xl">
      <LoginForm />
    </div>
  );
};

export default login;
