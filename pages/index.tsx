import type { NextPage } from 'next';
import React from 'react';
import Card from '../components/Card';
import Filter from '../components/Filter';
import Search from '../components/Search';
import Sort from '../components/Sort';
import { addToFavourites } from '../services/favourites/addToFavourites';
import { useGetMovies } from '../services/movies/getMovies';
import { IMovie } from '../services/movies/interfaces';

const Home: NextPage = () => {
  const [params, setParams] = React.useState({});
  const { data: movies } = useGetMovies({ params });

  const handleAddToFavourites = async (movie: IMovie) => {
    await addToFavourites(movie);
  };

  return (
    <main className="flex flex-col">
      <div className="flex justify-between items-center px-5 sm:px-10 pt-10 flex-wrap gap-2 mb-10">
        <Filter
          onFilterChange={(filterBy) =>
            setParams({ ...params, filter_by: filterBy })
          }
        />
        <Search
          onQueryChange={(query) => {
            setParams({ ...params, query });
          }}
        />
        <Sort
          className="ml-auto"
          onSortChange={(sortBy) => setParams({ ...params, sort_by: sortBy })}
        />
      </div>
      <div className="flex flex-wrap gap-3 p-4">
        {movies?.results.length === 0 ? (
          <div className="text-lg text-gray-400 font-bold text-center w-full p-20">
            There are no results.
          </div>
        ) : (
          movies?.results?.map((movie) => (
            <Card
              key={movie.id}
              title={movie.title}
              genres={movie.genre_ids}
              rate={movie.vote_average}
              releaseDate={movie.release_date}
              slotEnd={() => (
                <button
                  onClick={() => handleAddToFavourites(movie)}
                  className="px-6 py-3 text-white bg-blue-400 hover:opacity-90 rounded-full font-bold"
                >
                  Add to favourites
                </button>
              )}
            />
          ))
        )}
      </div>
    </main>
  );
};

export default Home;
