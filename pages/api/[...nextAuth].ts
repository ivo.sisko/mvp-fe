import NextAuth from "next-auth";
import Credentials from "next-auth/providers/credentials";

export default NextAuth({
    providers: [
      Credentials({
        name: "Email",
        credentials: {
          email: {
            label: "Email",
            type: "email",
            placeholder: "mail@domain.com",
          },
          password: { label: "Password", type: "password" },
        },
        async authorize(credentials) {
          // The 'url' is pointing to a Rails API endpoint which returns a JWT Token
          const url = `http://localhost:8080/auth/login`;
  
          const res = await fetch(url, {
            method: "POST",
            body: JSON.stringify(credentials),
            headers: {
              "Content-Type": "application/json",
            },
          });
          const user = await res.json();
  
          // If no error and we have user data, return it
          if (res.ok && user) {
            // I SAW EXAMPLES RETURNING {"email": "blah@tst.com"}
            return user; // MY CONTENT {token: 'eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo0LCJyb2xl…0.OAGiwjj9O_NsH02lIjA2D4HYZkmTQ3_SqtKcVgaIul0'}
          }
          // Return null if user data could not be retrieved
          return null;
        },
      }),
    ],
  
    callbacks: {
      async jwt({ token, user, account, isNewUser }) {// This user return by provider {} as you mentioned above MY CONTENT {token:}
        if (user) {
          if (user.token) {
            token = { accessToken: user.token };
          }
        }
        return token;
      },
  
      // That token store in session
      async session({ session, token }) { // this token return above jwt()
        session.accessToken = token.accessToken;
        //if you want to add user details info
        session.user = { name: "name", email: "email" };//this user info get via API call or decode token. Anything you want you can add
        return session;
      },
    },
  });