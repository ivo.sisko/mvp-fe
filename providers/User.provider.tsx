import React from 'react';
import { login } from '../services/auth/login';

export const UserContext = React.createContext<UserContext>({
  authenticated: false,
});

export const UserProvider: React.FC<any> = ({ children }) => {
  const [authenticated, setAuthenticated] = React.useState(false);
  const context = React.useMemo(
    () => ({
      authenticated,

      login: async (credentials: { email: string; password: string }) => {
        await login(credentials);
        setAuthenticated(true);
        return;
      },

      logout: () => {
        localStorage.removeItem('access_token');
        window.location.href = '/login';
      },
    }),
    [authenticated]
  );

  React.useEffect(() => {
    const token = localStorage.getItem('access_token');
    if (token) {
      setAuthenticated(true);
    }
  }, []);
  return (
    <UserContext.Provider value={context}>{children}</UserContext.Provider>
  );
};

export const useUserContext = () => {
  const context = React.useContext(UserContext);
  if (context === undefined) {
    throw new Error('User context must be used within a UserProvider');
  }
  return context;
};

interface UserContext {
  authenticated: boolean;
  login?: (credentials: { email: string; password: string }) => void;
  logout?: () => void;
}
