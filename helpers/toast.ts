import { toast } from 'react-toastify';

export function showToast(
  message: string,
  type: 'success' | 'warning' | 'error'
) {
  return toast[type](message, {
    position: 'bottom-right',
    autoClose: 1000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: false,
    draggable: true,
    progress: undefined,
  });
}
