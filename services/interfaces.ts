export interface ApiCollectionResponse<T> {
  page: number;
  results: T[];
  totalPages: number;
  totalResults: number;
}
