import { AxiosRequestConfig } from 'axios';
import { useQuery, UseQueryOptions } from 'react-query';
import { axiosApiInstance } from '../interceptors';
import { ApiCollectionResponse } from '../interfaces';
import { IMovie } from '../movies/interfaces';

export const getFavourites = async () => {
  return await axiosApiInstance
    .get<IMovie[]>('/favourites')
    .then((response) => {
      return Promise.resolve(response.data);
    })
    .catch((error) => {
      console.error('Error', error);
      return Promise.reject(error);
    });
};

export type IGetFavouritesOptions = UseQueryOptions<
  IMovie[],
  unknown,
  IMovie[]
>;

export function useGetFavourites(queryOptions?: any) {
  return useQuery(['favourites'], () => getFavourites(), queryOptions);
}
