import { axiosApiInstance } from '../interceptors';
import { IMovie } from '../movies/interfaces';

export const removeFromFavourites = async (id: string) => {
  return await axiosApiInstance
    .delete(`/favourites/delete/${id}`)
    .then((response) => {
      return Promise.resolve(response.data as any);
    })
    .catch((error) => {
      console.error('Error', error);
      return Promise.reject(error);
    });
};
