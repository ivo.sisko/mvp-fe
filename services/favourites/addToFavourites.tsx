import { toast } from 'react-toastify';
import { axiosApiInstance } from '../interceptors';
import { IMovie } from '../movies/interfaces';

export const addToFavourites = async (movie: IMovie) => {
  return await axiosApiInstance
    .post('/favourites', movie)
    .then((response) => {
      toast('Added to favourites!', {
        type: 'success',
      });
      return Promise.resolve(response.data as any);
    })
    .catch((error) => {
      console.error('Error', error);
      return Promise.reject(error);
    });
};
