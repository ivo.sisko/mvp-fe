import { AxiosRequestConfig } from 'axios';
import { useQuery, UseQueryOptions } from 'react-query';
import { axiosApiInstance } from '../interceptors';
import { ApiCollectionResponse } from '../interfaces';
import { IMovie } from './interfaces';

interface IGetMoviesConfig extends AxiosRequestConfig {
  params?: IGetMoviesQueryString;
}
type IGetMoviesQueryString = { 
  filter_by?: string | string[];
  sort_by?: string;
  query?: string
};

export const getMovies = async (config: IGetMoviesConfig) => {
  return await axiosApiInstance
    .get<ApiCollectionResponse<IMovie>>('/movies', config)
    .then((response) => {
      return Promise.resolve(response.data);
    })
    .catch((error) => {
      console.error('Error', error);
      return Promise.reject(error);
    });
};

export type IGetMoviesOptions = UseQueryOptions<
  ApiCollectionResponse<IMovie>,
  unknown,
  ApiCollectionResponse<IMovie>
>;

export function useGetMovies(
  config: IGetMoviesConfig,
  queryOptions?: any
) {
  return useQuery(
    ['movies', { ...config.params }],
    () => getMovies(config),
    queryOptions
  );
}
