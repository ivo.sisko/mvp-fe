import axios from 'axios';
import { axiosApiInstance } from '../interceptors';
import { User } from './interfaces';

export interface LoginBody {
  email: string;
  password: string;
}
export const login = async (body: LoginBody) => {
  return await axiosApiInstance
    .post('/auth/login', body)
    .then((response) => {
      localStorage.setItem('access_token', response.data.access_token);
      return Promise.resolve(response.data as User);
    })
    .catch((error) => {
      console.error('Error', error);
      return Promise.reject(error);
    });
};
