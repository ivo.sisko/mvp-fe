import axios from 'axios';
import { axiosApiInstance } from '../interceptors';
import { User } from './interfaces';

export interface RegisterBody {
  email: string;
  password: string;
  firstname: string;
  lastname: string;
}
export const register = async (body: RegisterBody, callback: any) => {
  return await axiosApiInstance
    .post('/auth/register', body)
    .then((response) => {
      Promise.resolve(response.data as User);
      callback();
      return;
    })
    .catch((error) => {
      console.error('Error', error);
      return Promise.reject(error);
    });
};
