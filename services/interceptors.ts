import axios from 'axios';
export const axiosApiInstance = axios.create();

// Request interceptor for API calls
axiosApiInstance.interceptors.request.use(
  async (config) => {
    const access_token = localStorage.getItem('access_token');
    config.baseURL = 'http://localhost:8080';
    config.headers = {
      Authorization: `Bearer ${access_token}`,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

// Response interceptor for API calls
axiosApiInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  async function (error) {
    const originalRequest = error.config;
    if (error.response.status === 401) {
      // originalRequest._retry = true;
      // const access_token = '';
      // axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
      // return axiosApiInstance(originalRequest);
      window.location.href = '/login';
    }
    return Promise.reject(error);
  }
);
